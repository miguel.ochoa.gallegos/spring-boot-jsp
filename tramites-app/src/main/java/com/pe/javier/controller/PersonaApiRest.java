package com.pe.javier.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pe.javier.repository.PersonaRespository;

@RestController
public class PersonaApiRest {
	
	@Autowired
	PersonaRespository personaRespository;
	
	@GetMapping("/personas")
	public ResponseEntity<?> getAll(){		
		return ResponseEntity.ok(personaRespository.findAll());		
	}

}
