package com.pe.javier.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/ttramite")
public class TipoTramiteController {
	
	@RequestMapping("/home")
	public String init() {
		return "mtto/ttramite";
	}

}
