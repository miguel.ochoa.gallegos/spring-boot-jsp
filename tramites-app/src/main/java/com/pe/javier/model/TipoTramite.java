package com.pe.javier.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class TipoTramite {

	@Id
	@GeneratedValue
	private Long tip_tra_id;
	private String tip_tra_descripcion;
	private String tip_tra_codigo;
	public Long getTip_tra_id() {
		return tip_tra_id;
	}
	public void setTip_tra_id(Long tip_tra_id) {
		this.tip_tra_id = tip_tra_id;
	}
	public String getTip_tra_descripcion() {
		return tip_tra_descripcion;
	}
	public void setTip_tra_descripcion(String tip_tra_descripcion) {
		this.tip_tra_descripcion = tip_tra_descripcion;
	}
	public String getTip_tra_codigo() {
		return tip_tra_codigo;
	}
	public void setTip_tra_codigo(String tip_tra_codigo) {
		this.tip_tra_codigo = tip_tra_codigo;
	}
}
