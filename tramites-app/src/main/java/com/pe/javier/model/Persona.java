package com.pe.javier.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Persona {

	@Id
	@GeneratedValue
	private Long per_id;	
	private String per_dni;	
	private String per_nombres;	
	private String per_ape_pat;	
	private String per_ape_mat;
	private Date per_fec_nac;
	
	public Long getPer_id() {
		return per_id;
	}
	public void setPer_id(Long per_id) {
		this.per_id = per_id;
	}
	public String getPer_dni() {
		return per_dni;
	}
	public void setPer_dni(String per_dni) {
		this.per_dni = per_dni;
	}
	public String getPer_nombres() {
		return per_nombres;
	}
	public void setPer_nombres(String per_nombres) {
		this.per_nombres = per_nombres;
	}
	public String getPer_ape_pat() {
		return per_ape_pat;
	}
	public void setPer_ape_pat(String per_ape_pat) {
		this.per_ape_pat = per_ape_pat;
	}
	public String getPer_ape_mat() {
		return per_ape_mat;
	}
	public void setPer_ape_mat(String per_ape_mat) {
		this.per_ape_mat = per_ape_mat;
	}
	public Date getPer_fec_nac() {
		return per_fec_nac;
	}
	public void setPer_fec_nac(Date per_fec_nac) {
		this.per_fec_nac = per_fec_nac;
	}	
}
