package com.pe.javier.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pe.javier.model.Persona;

@Repository
public interface PersonaRespository extends JpaRepository<Persona, Long>  {

}
